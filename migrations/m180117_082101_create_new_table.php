<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `new`.
 */
class m180117_082101_create_new_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'user_id' => Schema::TYPE_PK,
            'user_name' => Schema::TYPE_STRING . ' NOT NULL',
            'user_surname' => Schema::TYPE_STRING . ' NOT NULL',
            'user_bdate' => Schema::TYPE_INTEGER,
            'user_phone' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createTable('user_address', [
            'address_id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'address_name' => Schema::TYPE_STRING . ' NOT NULL',
            'address_description' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->addForeignKey(
            'user_id',
            'user_address',
            'user_id',
            'users',
            'user_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_address');
        $this->dropTable('users');
    }
}
