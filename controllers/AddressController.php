<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Address;

class AddressController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionEdit($user_id=0,$address_id=0)
    {
        $address=Address::findOne($address_id);
        if (Yii::$app->request->isPost && $address->load(Yii::$app->request->post())) $address->save();
        return $this->redirect(['user/edit','user_id'=>$user_id,'address_id'=>$address_id]);
    }

    public function actionAdd($user_id=0,$address_id=0)
    {
        $address=new Address();
        $address->user_id=$user_id;
        if (Yii::$app->request->isPost && $address->load(Yii::$app->request->post())) $address->save();
        return $this->redirect(['user/edit','user_id'=>$user_id,'address_id'=>$address_id]);
    }

}
