<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Address;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionList()
    {
        $users=User::find()->all();
        return $this->render('list', [
            'users' => $users,
        ]);
    }

    public function actionEdit($user_id,$address_id=0)
    {
        $user=User::findOne($user_id);
        if (Yii::$app->request->isPost && $user->load(Yii::$app->request->post())) $user->save();
        return $this->render('edit', [
            'user' => $user,
            'address' => (Yii::$app->request->get('act')=='addaddress'?new Address():$user->getAddress()->where(['=', 'address_id', $address_id])->limit(1)->one())
        ]);
    }

    public function actionAdd()
    {
        $user=new User();
        if (Yii::$app->request->isPost && $user->load(Yii::$app->request->post())) $user->save();
        return $this->render('add', [
            'user' => $user,
        ]);
    }

}
