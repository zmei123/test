<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

?>
<!--main content start-->
<section id="main-content">
<section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <?php 
                    $form = ActiveForm::begin([
                        'id' => 'form-signin',
                        'options' => ['class' => 'form-horizontal'],
                        'action' => Url::toRoute(['user/edit','user_id'=>$_GET['user_id']])
                    ]) ?>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Название компании</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_name', ['placeholder' => 'Имя *', 'class' => 'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Ссылка</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_surname', ['placeholder' => 'Фамилия *', 'class' => 'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Стоимость</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_phone', ['placeholder' => 'Телефон *', 'class' => 'form-control phone','required'=>'required','mask'=>'+7 (999) 999 99 99']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Дата рождения</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_bdate', ['placeholder' => 'Дата рождения *', 'class' => 'form-control bdate','required'=>'required','type'=>'date']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-danger">Сохранить</button>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                <a class="btn-mini btn-notice" href="<?= Url::toRoute(['user/edit','user_id'=>$_GET['user_id'],'act'=>'addaddress']); ?>">Добавить адрес</a><br>
                <?php if ($address) { ?>

                    <?php 
                    $form = ActiveForm::begin([
                        'id' => 'form-signin',
                        'options' => ['class' => 'form-horizontal'],
                        'action' => Url::toRoute(['address/'.(isset($_GET['act']) && $_GET['act']=='addaddress'?'add':'edit'),'user_id'=>$_GET['user_id'],'address_id'=>(isset($_GET['act']) && $_GET['act']=='addaddress'?0:$_GET['address_id'])])
                    ]) ?>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Название</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($address, 'address_name', ['placeholder' => 'Имя *', 'class' => 'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Ссылка</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($address, 'address_description', ['placeholder' => 'Фамилия *', 'class' => 'form-control','id' => 'Address','required'=>'required']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-danger">Сохранить</button>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>

                <?php } ?>
                <b>Адреса</b>
                    <table class="table table-bordered table-striped" id="myTable" style="font-size: 10px;">
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Название</th>
                                <th>Адрес</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user->address as $address) { ?>
                            <tr>
                                <td><?= $address->address_id; ?></td>
                                <td><?= $address->address_name; ?></td>
                                <td><?= $address->address_description; ?></td>
                                <td><button type="submit" class="btn btn-notice"><a class="btn-mini btn-notice" href="<?= Url::toRoute(['user/edit', 'user_id' => $user->user_id,'address_id'=>$address->address_id]); ?>">Редактировать</a></button></td>
                            </tr>
                            <?php } ?>
                        </tbody></table>

                    <?php $this->registerJs(
                        "$('#myTable').DataTable({
                            \"language\": {
                                \"url\": \"//cdn.datatables.net/plug-ins/1.10.7/i18n/Russian.json\"
                            },
                            \"order\": [[ 0, \"desc\" ]]
                        });",
                        View::POS_READY,
                        'my-button-handler'
                    ); ?>

                </section>
            </div>
        </div>

</section>
</section>

<?php $this->registerJs(
    "$(\"input.phone\").mask(\"(999)999-99-99\");

    $(\"#Address\").keyup(function(){
        //по мере ввода фразы, событие будет срабатывать всякий раз
        var search_query = $(this).val();
        //массив, в который будем записывать результаты поиска
        search_result = [];
        //делаем запрос к геокодеру
        $.getJSON('http://geocode-maps.yandex.ru/1.x/?format=json&geocode='+search_query, function(data) {
            //геокодер возвращает объект, который содержит в себе результаты поиска
            //для каждого результата возвращаются географические координаты и некоторая дополнительная информация
            //ответ геокодера легко посмотреть с помощью console.log();
            for(var i = 0; i < data.response.GeoObjectCollection.featureMember.length; i++) {
                //записываем в массив результаты, которые возвращает нам геокодер
                search_result.push({
                    label: data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                    value:data.response.GeoObjectCollection.featureMember[i].GeoObject.description+' - '+data.response.GeoObjectCollection.featureMember[i].GeoObject.name,
                    longlat:data.response.GeoObjectCollection.featureMember[i].GeoObject.Point.pos});
            }
            //подключаем к текстовому полю виджет autocomplete
            $(\"#Address\").autocomplete({
                //в качестве источника результатов указываем массив search_result
                source: search_result,
                select: function(event, ui){
                    //это событие срабатывает при выборе нужного результата
 
                }
            });
        });
    });

    ",
    View::POS_READY,
    'my-button-handler'
); ?>