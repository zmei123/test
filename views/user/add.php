<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

?>
<!--main content start-->
<section id="main-content">
<section class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <?php 
                    $form = ActiveForm::begin([
                        'id' => 'form-signin',
                        'options' => ['class' => 'form-horizontal'],
                        'action' => Url::toRoute(['user/add'])
                    ]) ?>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Имя</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_name', ['placeholder' => 'Имя *', 'class' => 'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Фамилия</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_surname', ['placeholder' => 'Фамилия *', 'class' => 'form-control','required'=>'required']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Телефон</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_phone', ['placeholder' => 'Телефон *', 'class' => 'form-control phone','required'=>'required','mask'=>'+7 (999) 999 99 99']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Дата рождения</label>
                            <div class="col-lg-10">
                                <?= Html::activeTextInput($user, 'user_bdate', ['placeholder' => 'Дата рождения *', 'class' => 'form-control bdate','required'=>'required','type'=>'date']); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-danger">Добавить</button>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </section>
            </div>
        </div>

</section>
</section>

<?php $this->registerJs(
    "$(\"input.phone\").mask(\"(999)999-99-99\");",
    View::POS_READY,
    'my-button-handler'
); ?>