<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;

?>
<a class="btn-mini btn-notice" href="<?= Url::toRoute(['user/add']); ?>">Добавить</a>
<table class="table table-bordered table-striped" id="myTable" style="font-size: 10px;">
    <thead>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Телефон</th>
            <th>Дата рождения</th>
            <th>Действия</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($users as $user) { ?>
    	<tr>
    		<td><?= $user->user_id; ?></td>
    		<td><?= $user->user_name; ?></td>
    		<td><?= $user->user_surname; ?></td>
    		<td><?= $user->user_phone; ?></td>
    		<td><?= $user->user_bdate; ?></td>
    		<td><button type="submit" class="btn btn-notice"><a class="btn-mini btn-notice" href="<?= Url::toRoute(['user/edit', 'user_id' => $user->user_id]); ?>">Редактировать</a></button></td>
    	<?php } ?>
    	</tr>
    </tbody></table>

<?php $this->registerJs(
    "$('#myTable').DataTable({
        \"language\": {
            \"url\": \"//cdn.datatables.net/plug-ins/1.10.7/i18n/Russian.json\"
        },
        \"order\": [[ 0, \"desc\" ]]
    });",
    View::POS_READY,
    'my-button-handler'
); ?>
