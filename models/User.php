<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\Company;
use app\helpers\Email;

class User extends ActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function tableName()
    {
        return 'users';
    }

    public function attributeLabels()
    {
        return array(
            'user_id' => 'user_id',
            'user_name' => 'Имя',
            'user_surname' => 'Фамилия',
            'user_bdate' => 'Дата рождения',
            'user_phone' => 'Телефон',
        );
    } 

    public function rules() {
        return [
                [['user_name','user_surname','user_bdate','user_phone'], 'required']
            ]; 
    }

    public function getAddress()
    {
        return $this->hasMany(Address::className(), ['user_id' => 'user_id']);
    }

}
