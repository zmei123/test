<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\Company;
use app\helpers\Email;

class Address extends ActiveRecord
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function tableName()
    {
        return 'user_address';
    }

    public function attributeLabels()
    {
        return array(
            'address_id' => 'address_id',
            'user_id' => 'user_id',
            'address_name' => 'Название',
            'address_description' => 'Адрес',
        );
    } 

    public function rules() {
        return [
                [['address_name','address_description'], 'required']
            ]; 
    }

}
